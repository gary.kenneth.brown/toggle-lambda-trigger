const AWS = require('aws-sdk');

exports.handler = async (event) => {
    
    var lambda =  new AWS.Lambda({apiVersion: '2015-03-31'});
    
    var pushListenerParams = {
        UUID: event.uuid,
        Enabled: event.enabled
     };
    console.log("Lambda subscription settings: " + JSON.stringify(pushListenerParams));
    
    // https://docs.aws.amazon.com/lambda/latest/dg/API_UpdateEventSourceMapping.html
    lambda.updateEventSourceMapping(pushListenerParams, function(err, res) {
        if(err) console.log('Push Listener Error => ' + JSON.stringify(err));
        else console.log('Push Listener Response => ' + JSON.stringify(res));
     });
    
    const response = {
        statusCode: 200,
        body: JSON.stringify('Lambda subscription status: ' + event.subscriptionState),
    };
    return response;
};
