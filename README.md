# toggle lambda trigger
Lambda to enable/disable another lambda's trigger such as an SQS event.

Reasons for this:

* Do not process data out of hours
* 3rd party cannot handle requests outside set hours
* Batching up work to then release when batch is ready
* Disable part of a system during maintenance/outage/performance load such as a circuit breaker pattern

![Toggle Trigger](Toggle_Lambda_Trigger.png)

## Getting started

1. Create a new NodeJS v14 Lambda (for reference we'll name this `my-toggle-lambda` in this document)
2. Set the IAM policy for `my-toggle-lambda` so it has UpdateEventSourceMapping permission on the target SQS subscription UUID.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "1",
            "Effect": "Allow",
            "Action": "lambda:UpdateEventSourceMapping",
            "Resource": "arn:aws:lambda:eu-west-2:652163390647:event-source-mapping:<UUID>"
        }
    ]
}
```

3. Define a cloudwatch scheduled trigger or event trigger to run `my-toggle-lambda` with the parameters `uuid` and `enabled`. Example JSON body to disable the target trigger:
```
{
  "uuid": "<UUID>",
  "enabled": false
}
```

Example JSON body to enable the target trigger:
```
{
  "uuid": "<UUID>",
  "enabled": true
}
```

4. Create the SQS and trigger Lambda as normal. Grab the UUID from SQS for the Lambda trigger and add this to the above cloudwatch trigger body.
